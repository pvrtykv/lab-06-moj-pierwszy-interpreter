:- module(forth_interpreter, [eval_program/3]).
:- use_module(forth_state).

/**
 * eval_program(+Program:list, +InitialState:term, -FinalState:term) is det
 * 
 * wykonuje wyrażenia z zadanej listy, startując w zadanym stanie interpretera
 * 
 * @arg Program lista wyrażeń forth
 * @arg InitialState początkowy stan intepretera
 * @arg FinalState stan interpretera po wykonaniu programu
 * 
 * @tbd interpretacja programu to nic innego jak foldl zaczynający od InitialState
 *      i używający predykatu state_change do złożenia listy;
 *      zrób to! :D
 */         
eval_program(Program, InitialState, FinalState) :- fail.

state_change(Statement, State, NextState) :-
    % once jest po to, żeby uniknąć przypadkowego uruchomienia 'catch-all' (ostatnie eval_statement)
    once(eval_statement(Statement, State, NextState)).

% jedno (najprostsze) wyrażenie jest zaimplementowane jako przykład
eval_statement(statement(push(Number)), State, NextState) :-
    state_stack_push(State, Number, NextState).

eval_statement(statement(arithmetic(OP)), State, NextState) :-
    % OP to operator ze zbioru {+, -, /, *, mod}
    %
    % TODO:
    % 1. zdejmij dwa pierwsze elementy (N1 i N2) ze stosu (state_stack_pop/4)
    % 2. zbuduj wyrażenie arytmetyczne OP(N1, N2), użyj "=.."
    % 3. wykonaj wyrażenie używając "is"
    % 4. wrzuć wynik na stos (state_stack_push/3)
    fail.

% Comparisons
eval_statement(statement(comparison(OP)), State, NextState) :-
    % OP to operator ze zbioru {=, <, >}
    %
    % TODO:
    % 1. zdejmij dwa pierwsze elementy (N1 i N2) ze stosu (state_stack_pop/4)
    % 2. zbuduj wyrażenie porównujące OP(N1, N2), użyj "=.."
    % 3. wykonaj wyrażenie używając "call"
    % 4a. jeżeli call się uda, wrzuć na stos -1
    % 4b. w innym wypadku wrzuć 0 (state_stack_push/3)
    fail.

% Logic
eval_statement(statement(and), State, NextState) :-
    % TODO:
    % 1. zdejmij dwa pierwsze elementy (B1 i B2) ze stosu (state_stack_pop/4)
    % 2. sprawdz, czy oba B1 i B2 mają wartości różne od 0 
    % 3a. jeżeli tak, wrzuć na stos -1
    % 3b. w innym wypadku wrzuć 0
    fail.

eval_statement(statement(or), State, NextState) :-
    % TODO:
    % 1. zdejmij dwa pierwsze elementy (B1 i B2) ze stosu (state_stack_pop/4)
    % 2. sprawdz, czy oba B1 i B2 mają wartości równe 0 
    % 3a. jeżeli tak, wrzuć na stos 0
    % 3b. w innym wypadku wrzuć -1
    fail.

eval_statement(statement(invert), State, NextState) :-
    % TODO:
    % 1. zdejmij jeden element (B1 ) ze stosu (state_stack_pop/3)
    % 2. sprawdz, czy B1 jest równe 0 
    % 3a. jeżeli tak, wrzuć na stos -1
    % 3b. w innym wypadku wrzuć 0
    fail.

% Stack manipulation
eval_statement(statement(dup), State, NextState) :-
    % TODO:
    % 1. zdejmij jeden element I ze stosu (state_stack_pop/3)
    % 2. wrzuć go dwukrotnie na stos (state_stack_push/4)
    fail.

eval_statement(statement(drop), State, NextState) :-
    % TODO:
    % 1. zdejmij jeden element ze stosu i go zignoruj
    fail.

eval_statement(statement(swap), State, NextState) :-
    % TODO:
    % 1. zdejmij dwa elementy ze stosu
    % 2. wrzuć je na stos w odwrotnej kolejności
    fail.

eval_statement(statement(over), State, NextState) :-
    % TODO:
    % 1. zdejmij dwa elementy ze stosu
    % 2. zduplikuj jeden z nich i wrzuć je w odpowiedniej kolejności na stos
    fail.

eval_statement(statement(rot), State, NextState) :-
    % TODO:
    % 1. zdejmij trzy elementu ze stosu (state_stack_pop/5)
    % 2. wrzuć je w odpowiedniej kolejności na stos
    fail.

% Output
eval_statement(statement(print), State, NextState) :-
    % TODO:
    % 1. zdejmij jeden element ze stosu
    % 2. wypisz go (write/1)
    fail.
eval_statement(statement(emit), State, NextState) :-
    % TODO:
    % 1. zdejmij jedną liczbę ze stosu
    % 2. przetłumacz ją na znak ASCII (char_code/2)
    % 3. wypisz znak (write/1)
    fail.
eval_statement(statement(cr), State, State) :-
    % TODO:
    % 1. wypisz znak nowej linii
    fail.
eval_statement(statement(print_string(String)), State, State) :-
    % TODO:
    % 1. wypisz zadany Napis
    fail.

% Input
eval_statement(statement(key), State, NextState) :-
    % TODO:
    % 1. pobierz jeden znak z klawiatury (get_single_char/1)
    % 2. umieść go na stosie
    fail.

% Env Dependent Statements
eval_statement(statement(define_fun(Name, Fun)), State, NextState) :-
    % TODO:
    % 1. umieść fun(Fun) w środowisku pod kluczem Name (state_env_set/4)
    fail.

eval_statement(statement(define_var(Name)), State, NextState) :-
    % TODO:
    % 1. zaalokuj pamięć na zmienną (state_mem_alloc/3)
    % 2. umieść adres zaalokowanej komórki w środowisku pod kluczem Name (state_env_set/4)
    fail.

eval_statement(statement(define_const(Name)), State, NextState) :-
    % TODO:
    % 1. zdejmij wartość ze stosu
    % 2. zapisz ją w środowisku pod kluczem Name
    fail.

eval_statement(statement(call_env(Name)), State, NextState) :-
    % TODO:
    % 1. pobierz ze środowiska wartość pod kluczem Name
    % 2a. jeżeli wartość jest opakowana w fun/1, wykonaj ją (eval_program/3)
    % 2b. w innym wypadku wstaw wartość na stos
    fail.

% Address handling
eval_statement(statement(deref), State, NextState) :-
    % TODO:
    % 1. zdejmij adres zmiennej ze stosu (state_stack_pop/3)
    % 2. pobierz wartość zmiennej z pamięci z tego adresu (state_mem_get/3)
    % 3. umieść wartość na stosie (state_stack_push/3)
    fail.

eval_statement(statement(set), State, NextState) :-
    % TODO:
    % 1. zdejmij adres i wartość ze stosu
    % 2. ustaw wartość na danym adresie w pamięc (state_mem_set/4)
    fail.

eval_statement(statement(modify_var(OP)), State, NextState) :-
    % OP to operator arytmetyczny ze zbioru {+, -, /, *, mod}
    % To wyrażenie działa odpowiednio jak +=, -=, /=, *=, mod=
    % i służy do szybkiej modyfikacji wartości zmiennych (+! w EasyForth)
    % Zaimplementujemy je w formie makra!
    %
    % TODO:
    % 1. stwórz program (listę wyrażeń) z już zaimplementowanych wyrażeń, który odda zadany efekt
    % 2. wykonaj ten program (eval_program/3)
    fail.	

eval_statement(statement(print_var), State, NextState) :-
    % Kolejne makro, która ma wypisać wartość zmiennej pod danym adresem
    %
    % TODO:
    % 1. stwórz program, który realizuje to zadanie
    % 2. wykonaj go (eval_program/3)
    fail.		

% Conditionals
eval_statement(statement(ifelse(Then, Else)), State, NextState) :-
    % TODO:
    % 1. pobierz wartość ze stosu
    % 2a. jeżeli wartość jest 0, wykonaj program Else (eval_program/3)
    % 2b. w innym wypadku wykonaj program Then (eval_program/3)
    fail.

eval_statement(statement(if(Then)), State, NextState) :-
    % TODO:
    % if to to samo co ifelse z pustym programem Else.
    fail.

% Loops    
eval_statement(statement(until(Body)), State, NextState) :- 
    % pętla until, coś w stylu do while w C :)
    % TODO:
    % 1. wykonaj program Body (eval_program/3)
    % 2. zdejmij wartość ze stosu
    % 3a. jeżeli wartość to 0 (false), to rekurencyjnie wykonaj eval_statement z nowym stanem
    % 3b. w innym wypadku, aktualny stan do końcowy stan
    fail.
    
eval_statement(statement(loop(Body)), State, NextState) :-
    % pętla loop, coś w stylu for z C :)
    % TODO:
    % 1. pobierz ze stosu dwie wartości (wartość początkowa i końcowa pętli) 
    % 2. wrzuć do środowiska (stack_eval_set/3) wartość początkową pod klucz i
    % 3. wykonaj program Body (eval_program/3)
    % 4. zinkrementuj wartość 'i' 
    % 5a. jeżeli wartość jest wyższa/równa wartości końcowej, to usuwamy 'i' ze środowiska i kończymy robotę
    % 5b. w innym wypadku zaktualizuj środowisko o nową wartość i wróc do punktu 3
    %
    % tip. możesz chcieć wypakować część kodu do osobnego predykatu (eval_loop/5), przygotowanego na dole pliku
    fail.

eval_statement(statement(i), State, NextState) :-
    % TODO:
    % 1. pobierz ze środowiska wartość pod kluczem 'i'
    % 2. umieść tę wartość na stosie
    fail. 

% to wyrazenie wypisuje aktualny stan interpretera,
% przydatne do debugowania
eval_statement(statement(debug), State, State) :-
    state_debug(State).

% nie usuwaj tej klauzulu, ona w założeniu ma pomóc znalezc bledy
% poprzez wykrycie nieobsluzonych wyrazen 
eval_statement(Statement, State, State) :-
    format('WARNING: an unrecognized statement has been ignored: ~w\n', [Statement]),
    state_debug(State).

% helper do implementacji pętli 'loop', może być warto użyć tego kodu
eval_loop(Value, To, Body, State, FinalState) :- fail.

