:- use_module(forth).

increment_variable :- forth([
    statement(define_var(balance)),
    statement(push(123)),
    statement(call_env(balance)),
    statement(set),
    statement(call_env(balance)),
    statement(print_var),
    statement(cr),
    statement(push(50)),
    statement(call_env(balance)),
    statement(modify_var(+)),
    statement(call_env(balance)),
    statement(print_var)], _).

answer :- forth([
    statement(push(42)),
    statement(define_const(answer)),
    statement(push(2)),
    statement(call_env(answer)),
    statement(arithmetic(*)),
    statement(debug)
    ], _).

fizz_buzz :- forth([
    statement(
        define_fun('fizz?', [
            statement(push(3)), 
            statement(arithmetic(mod)),
            statement(push(0)), 
            statement(comparison(=)), 
            statement(dup), 
            statement(if([
                statement(print_string("Fizz"))
                ]
            ))])),
       statement(
           define_fun('buzz?', [
               statement(push(5)), 
               statement(arithmetic(mod)), 
               statement(push(0)), 
               statement(comparison(=)), 
               statement(dup), 
               statement(if([
                   statement(print_string("Buzz"))
                   ]
                ))])),
       statement(
           define_fun('fizz-buzz?', [
               statement(dup), 
               statement(call_env('fizz?')), 
               statement(swap), 
               statement(call_env('buzz?')), 
               statement(or), 
               statement(invert)
               ]
               )),
       statement(
           define_fun('do-fizz-buzz', [
               statement(push(25)), 
               statement(push(1)), 
               statement(loop([
                   statement(cr), 
                   statement(i), 
                   statement(call_env('fizz-buzz?')), 
                   statement(if([
                       statement(i), 
                       statement(print)
                       ]
                    ))]))])),
        statement(call_env('do-fizz-buzz'))
      ], _).

input_until_space :- forth([
    statement(
        until([
            statement(key), 
            statement(dup), 
            statement(print), 
            statement(push(32)), 
            statement(comparison(=))
            ])
        )
    ], _). 