:- module(forth, [forth/2]).

:- use_module(forth_state).
:- use_module(forth_interpreter).

/**
 * forth(+Program:list, -ResultState:term) is det
 * 
 * uruchamia program w Forth i zwraca stan po jego wykonaniu
 * 
 *  @arg Program lista wyrażeń 
 *  @arg ResultState stan interpretera po wykonaniu programu
 */ 
forth(Program, ResultState) :-
    state_empty(InitialState),
    eval_program(Program, InitialState, ResultState).