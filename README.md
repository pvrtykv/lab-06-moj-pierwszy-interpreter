# Lab 06. Mój pierwszy interpreter

Celem tego laboratorium jest stworzenie interpretera zabawkowej wersji języka Forth.

## Materiały

Opis języka, który będzie implementować: [EasyForth](https://skilldrick.github.io/easyforth/).

<details><summary>[tablice asocjacyjne](https://swish.swi-prolog.org/?code=https://gitlab.com/agh-courses/2020-2021/prolog/lab-06-moj-pierwszy-interpreter/-/raw/master/notebooks/12_tablice_asocjacyjne.swinb)</summary>
<ul>
<li> [alternatywny link 1](http://lpsdemo.interprolog.com/?code=https://gitlab.com/agh-courses/2020-2021/prolog/lab-06-moj-pierwszy-interpreter/-/raw/master/notebooks/12_tablice_asocjacyjne.swinb)
<li> [alternatywny link 2](http://cplint.ml.unife.it?code=https://gitlab.com/agh-courses/2020-2021/prolog/lab-06-moj-pierwszy-interpreter/-/raw/master/notebooks/12_tablice_asocjacyjne.swinb)
</details>

## Zadania

Proszę uzupełnić pliki `forth_interpreter` i `forth_state` z katalogu `forth` zgodnie z komentarzami `TODO:`.

Można posłużyć się plikiem `examples.pl`, który zawiera trzy przykłady kodu, który można odpalić w celu przetestowaniu interpretera. 

## GitLab Setup 

* [ ] Upewnij się, że masz **prywatną** grupę 
  * [jak stworzyć grupę?](https://docs.gitlab.com/ee/user/group/#create-a-group)
* [ ] Dodaj @bobot-is-a-bot jako nowego członka grupa (rola: **maintainer**)
  * [jak dodać członka do grupy?](https://docs.gitlab.com/ee/user/group/#add-users-to-a-group)
* [ ] Stwórz fork tego projekty wewnątrz swojej **prywatnej** grupy
  * [jak stworzyć forka?](https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html#creating-a-fork)

## Jak wysyłać rozwiązania

* [ ] Zklonuj swojego forka:
    ```bash 
    git clone <repository url>
    ```
* [ ] Rozwiąż zadania:
* [ ] Zacommituj swoje zmiany
    ```bash
    git add <path to the changed files>
    git commit -m <commit message>
    ```
* [ ] Wypchnij zmiany na gałąź **master**
    ```bash
    git push -u origin master
    ```

Resztą zajmie się Bobot. Możesz sprawdzić plik `GRADE.md`, żeby zobaczyć swoje wyniki. Plik `GRADE.md` aktualizowany jest w odstępach 15 min.

PS Bobot nie zna niestety języka polskiego i czasem mu się gubią polskie znaki.

